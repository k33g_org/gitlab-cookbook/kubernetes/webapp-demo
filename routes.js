async function routes (fastify, options) {
  
  fastify.log.info(`🤖 options routes:`)
  fastify.log.info(options)
  
  fastify.get(`/hello`, async (request, reply) => {
    return {
		message: `👋 Hello world 🌍`,
		pod: options.podName
	}
  })
    
}

module.exports = routes
